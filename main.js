app = {
	
	//INNIT FUNCTIONS
	init:function(){
		
		//Call bind events
		app.bindEvents();
		
		app.animateCC();
		
	},
	
	//EVENT BINDINGS
	bindEvents:function(){
		
	},
	
	//FUNCTIONS

	animateCC:function() {		console.log('Running app.animate()');
		
		//Check all canvas on the page that are animateCC objects
		$('canvas.aniCC').each(function(){		console.log('Getting animation: '+$(this).data('ani'));
			
			var thisAni = $(this).data('ani');//Get the animation name formt he data tag
			
			canvas = $(this)[0]; //Assign this canvas object (must be raw Js not jQ obj)
			
			exportRoot = new window[thisAni].canvas(); //Assign this animation

			fps = window[thisAni].properties.fps; //Get the fps
			
			stage = new createjs.Stage(canvas); //Setup the stage
			stage.addChild(exportRoot); //Link the animation
			stage.update(); //Update the stage
			
			createjs.Ticker.setFPS(fps); //Get properties
			createjs.Ticker.addEventListener("tick", stage); //Play the animation
			
			//Assign yhe other properties
			$(canvas).attr('width', window[thisAni].properties.width);
			$(canvas).attr('height', window[thisAni].properties.height);
			$(canvas).css({
				'background-color': window[thisAni].properties.color
			});

		});
	},
	
};//END app();

//DOC READY
jQuery(document).ready(function() { 
	app.init();
});

//WINDOW RESIZE
$(window).resize(function() {
	
});