#Adobe AnimateCC Function
>**How to Integrate Adobe AnimateCC canvas into a regular app structure.**

AnimateCC is the new version of Flash that exports to `<canvas>` objects rather than `.swf`.

This guide deals with how to make use of the exported files in a live site environment.

Upon publishing from CC you will find two files

_(where "aniName" is the original source file)_

1. **`aniName.html`** - contains the canvas object and a singular Js function used to initiate the animation.
2.  **`aniName.js`** - contains the animations parameters, constructors and timeline.

In the the `aniName.html` you will find a function tor run the animation.

```javascript
var canvas, stage, exportRoot;
function init() {

	canvas = document.getElementById("canvas");
	exportRoot = new lib.aniname();

	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);
	stage.update();

	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);
}
```

###The Problems
The issue with this basic code comes when you want to include multiple animations on a page/site.
1. All `<canvas>` objects must be given unique `id`'s
2. All `aniName.js` files must be included as separate links
3. each `<canvas>` must have its own function

###The Solution
This demo provides the following solutions
1. Canvas objects link to animations via a `data` attribute
2. Animation codes can be combined into a single minimized file `animations.js`
3. A global function checks all `<canvas>` objects and assigns the correct animation to each.

--- 

####Step 1 - Publish and prepare
Publish all your animations from CC and copy and paste all the `aniName.js` files contents into a new `animation.js`. This can be minimized if you wish.

Open your sites `index.html` and add the following scripts as links.

```html
<!--Jquery of your choice -->
<script type="text/javascript" src="Jquery"></script>
<!-- CreateJS.min -->
<script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
<!--Your Sites Main Js File -->
<script type="text/javascript" src="main.js"></script>
<!--Your combined animations -->
<script type="text/javascript" src="animations.js"></script>
```

####Step 2 - Modify the `animation.js`
In order to make each animation unique so the function can get the right properties we need to modify to ever so slightly.

For each animation in the animations JS you need to change three things.

1. Search for the comment `// stage content` and change the following line to read `lib.canvas = ...`
```javascript
// stage content
lib.aniName = function(...
```
becomes
```javascript
// stage content
lib.canvas = function(...
```
2. Look for the last two lines of the animation an change the three uses of the  `lib` variables to match your `aniName`.
```javascript
...
})(lib = lib1||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;
```
becomes
```javascript
...
})(aniName = aniName||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var aniName, images, createjs, ss;
```

>_this step can also be done before publishing by visiting the `publish_settings > Advanced (tab) > symbols` and changind the `lib` value to your `aniName`._

Your animation is now a unique object that the function can differentiate between. You can now safely minimize your `animations.js` if you wish.

####Step 3 - Place the Canvas'

In your HTML you will need to place all your <canvas> elements. Each canvas will need to following:

1. `class="aniCC"` to distinguish it from any other canvas elements used on the site for other purposes
2. `data-ani="aniName"` A unique data tag that matches the name of the specific animation you want to place in it.

```html
<canvas class="aniCC" data-ani="ani1"></canvas>
```

####Step 4 - include the function
The following function is designed to be included as part of the Tommy approved `app()`
structure for javascript.

Place the following function in your `main.js` app structure:

```javascript
animateCC:function() {
	
	$('canvas.aniCC').each(function(){			
		
		var thisAni = $(this).data('ani');
			
		canvas = $(this)[0];
			
		exportRoot = new window[thisAni].canvas();

		fps = window[thisAni].properties.fps;
			
		stage = new createjs.Stage(canvas);
		stage.addChild(exportRoot);
		stage.update();
			
		createjs.Ticker.setFPS(fps);
		createjs.Ticker.addEventListener("tick", stage);
			
		$(canvas).attr('width', window[thisAni].properties.width);
		$(canvas).attr('height', window[thisAni].properties.height);
		$(canvas).css({
			'background-color': window[thisAni].properties.color
		});

	});
},
```

You can now initiate the function by running `app.animateCC()` either from your `app.init()` or by binding it to another event.

Basically the function finds all the canvas objects with the correct class of `aniCC`.  For each case it grabs the animation name form the data  tag and assigns it to the current `<canvas>`. It then assigns the properties to the `<canvas>`.

---

##Next Version..
The next version of this function will include support for embedded images and web-fonts.

---

> Written by **Mike Dove** Feb 2016 –  mike@thisistommy.com
> Special thanks to Farhad and Carlos for JS help.