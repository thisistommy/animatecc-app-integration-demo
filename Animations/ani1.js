(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 200,
	height: 200,
	fps: 24,
	color: "#999999",
	webfonts: {},
	manifest: []
};



lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



// stage content:
(lib.ani1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape.setTransform(100,100);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.957)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_1.setTransform(100,100);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.918)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_2.setTransform(100,100);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(0,0,0,0.875)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_3.setTransform(100,100);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(0,0,0,0.835)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_4.setTransform(100,100);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(0,0,0,0.792)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_5.setTransform(100,100);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(0,0,0,0.753)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_6.setTransform(100,100);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.71)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_7.setTransform(100,100);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(0,0,0,0.667)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_8.setTransform(100,100);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.627)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_9.setTransform(100,100);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.584)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_10.setTransform(100,100);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(0,0,0,0.545)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_11.setTransform(100,100);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(0,0,0,0.502)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_12.setTransform(100,100);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("rgba(0,0,0,0.459)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_13.setTransform(100,100);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(0,0,0,0.42)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_14.setTransform(100,100);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(0,0,0,0.376)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_15.setTransform(100,100);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(0,0,0,0.337)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_16.setTransform(100,100);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("rgba(0,0,0,0.294)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_17.setTransform(100,100);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(0,0,0,0.255)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_18.setTransform(100,100);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("rgba(0,0,0,0.212)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_19.setTransform(100,100);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("rgba(0,0,0,0.169)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_20.setTransform(100,100);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(0,0,0,0.129)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_21.setTransform(100,100);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("rgba(0,0,0,0.086)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_22.setTransform(100,100);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("rgba(0,0,0,0.047)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_23.setTransform(100,100);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(0,0,0,0.004)").s().p("Au0O2IAA9qIdqAAIAAdqg");
	this.shape_24.setTransform(100,100);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(105,105,190,190);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;